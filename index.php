<?php

use app\Test;


ini_set('memory_limit', '128M');
spl_autoload_register(
    function ($className) {
        $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
        $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . $className . '.php';
        if (file_exists($path)) {
            require_once $path;

            return true;
        }

        return false;
    }
);

Test::execute();