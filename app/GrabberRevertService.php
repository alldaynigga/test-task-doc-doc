<?php

namespace app;

use app\GrabberService;
use app\DIContainer;

/**
 * Class GrabberRevertService
 * @package app\GrabberService
 */
class GrabberRevertService extends GrabberService
{
    /**
     * @return self
     */
    public function replaceRevert(): self
    {
        $rules = array_flip($this->replaceRules);
        /** @type Replacer $replacer */
        $replacer = DIContainer::getInstance()->get(Replacer::class, [$rules, $this->text]);
        $this->text = $replacer->getReplacedText();

        return $this;
    }
}