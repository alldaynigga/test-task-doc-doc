<?php

namespace app;


class Replacer
{

    /**
     * @var array
     */
    protected $replaceRules;

    /**
     * @var
     */
    protected $text;

    public function __construct(array $replaceRules = [], string $text = '')
    {
        $this->replaceRules = $replaceRules;
        $this->text = $text;
    }

    /**
     * Replace method
     *
     * @return string
     * @throws \Error
     */
    public function getReplacedText(): string
    {
        $step = 0;
        $limit = count($this->replaceRules);
        foreach ($this->replaceRules as $pattern => $replace) {
            $this->text = preg_replace("#$pattern#u", $replace, $this->text);
            if ($step > $limit) {
                throw new \Error('Endless replace cycle');
            }
            $step++;
        }

        return $this->text;
    }
}