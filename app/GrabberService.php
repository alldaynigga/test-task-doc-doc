<?php

namespace app;

use app\DIContainer;
use app\Crawler;
use app\Replacer;

/**
 * Class GrabberService
 * @package app\GrabberService
 */
class GrabberService implements \Countable
{

    protected const PARAM_CONNECT_TIMEOUT = 10;
    protected const PARAM_TIMEOUT = 10;

    /**
     * Url param
     *
     * @var
     */
    protected $url;

    /**
     * Replace rules
     *
     * @var array
     */
    protected $replaceRules = [];

    /**
     * Timeout
     *
     * @var
     */
    protected $timeOut;

    /**
     * @var
     */
    protected $text;

    /**
     * Setter for url property
     *
     * @param string $url
     * @return GrabberService
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return self
     */
    public function scanUrl(): self
    {
        try {

            /** @type Crawler $crawler */
            $crawler = DIContainer::getInstance()->get(Crawler::class, [$this->url]);
            $crawler->addOption(CURLOPT_RETURNTRANSFER, true);
            $crawler->addOption(CURLOPT_TIMEOUT, self::PARAM_TIMEOUT);
            $this->text = $crawler->scan();

            return $this;
        } catch (\Exception $e) {
            $message = $e->getMessage() . $e->getTraceAsString();
            error_log($message);
        }
    }

    /**
     * Setter for replace property
     *
     * @param array $replaceRules
     * @return GrabberService
     */
    public function setReplace(array $replaceRules): self
    {
        $this->replaceRules = $replaceRules;

        return $this;
    }

    /**
     * @return GrabberService
     */
    public function replace(): GrabberService
    {
        /** @type Replacer $replacer */
        $replacer = DIContainer::getInstance()->get(Replacer::class, [$this->replaceRules, $this->text]);
        $this->text = $replacer->getReplacedText();

        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * Count method
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->replaceRules);
    }
}