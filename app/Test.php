<?php

namespace app;

/**
 * Class Test
 * @package app
 */
class Test
{

    /**
     * Param url
     */
    public const PARAM_URL = 'https://www.python.org';

    /**
     * Param replaces
     */
    public const PARAM_REPLACE_RULES = ['Python' => 'php', 'python' => 'php', 'function' => 'method'];

    /**
     * Test for example
     */
    public static function execute(): void
    {
        /** @type GrabberRevertService $grabber */
        $grabber = new GrabberRevertService();
        $result1 = $grabber->setUrl(self::PARAM_URL)
            ->setReplace(self::PARAM_REPLACE_RULES)
            ->scanUrl()
            ->replace()
            ->getText();

        $result2 = $grabber->replaceRevert()->getText();

        var_dump($result1, $result2);
    }

}
