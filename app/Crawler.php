<?php

namespace app;


/**
 * Class Crawler
 * @package app\GrabberService
 */
class Crawler
{
    /**
     * Default Allowed Http Codes
     */
    protected const ALLOWED_HTTP_CODES = [200, 100, 301, 201];

    /**
     * @var resource
     */
    protected $resource;

    /**
     * Http Codes Allowed
     *
     * @var
     */
    protected $allowedHttpCodes;

    /**
     * Headers List
     *
     * @var array
     */
    protected $headers = [];

    /**
     * Crawler constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->resource = curl_init($url);
    }

    /**
     * Setter for allowedHttpCodes
     *
     * @param array $codes
     * @return Crawler
     */
    public function setAllowedHttpCodes(array $codes): self
    {
        $this->allowedHttpCodes = $codes;
    }

    /**
     * Add header method
     *
     * @param string $header
     * @return Crawler
     */
    public function addHeader(string $header): self
    {
        $this->headers[] = $header;

        return $this;
    }

    /**
     * Add custom Option
     *
     * @param int $option
     * @param $value
     * @return Crawler
     */
    public function addOption(int $option, $value): self
    {
        curl_setopt($this->resource, $option, $value);

        return $this;
    }


    /**
     * @return string
     * @throws \Exception
     */
    public function scan(): string
    {
        try {
            if (!empty($this->headers)) {
                curl_setopt($this->resource, CURLOPT_HTTPHEADER, $this->headers);
            }
            $output = curl_exec($this->resource);
            $info = curl_getinfo($this->resource);
            curl_close($this->resource);
        } catch (\Exception $e) {
            error_log($e->getMessage() . $e->getTraceAsString());
        }

        if (empty($info) || empty($info['http_code']) || empty($output) || !in_array($info['http_code'],
                self::ALLOWED_HTTP_CODES) || !is_string($output)) {
            throw new \Exception('Wrong response');
        }

        return $output;
    }
}