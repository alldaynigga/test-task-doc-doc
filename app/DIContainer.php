<?php

namespace app;


/**
 * Class DIContainer
 * @package app
 */
class DIContainer
{
    /**
     * @var null
     */
    private static $instance = null;

    /**
     * DIContainer constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return DIContainer|null
     */
    public
    static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $className
     * @param array $params
     *
     * @return mixed
     * @throws \Exception
     */
    public function get(string $className, array $params = [])
    {
        if (!class_exists($className)) {
            throw new \Exception ("cant load class with name: $className");
        }
        if (!empty($params)) {
            return new $className(...$params);
        }

        return new $className();
    }

}